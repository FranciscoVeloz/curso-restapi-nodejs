create database notesdb;
use notesdb;

create table notes (
    id int primary key auto_increment,
    title varchar(50),
    description varchar(250)
);

insert into notes values
(null, 'Note 1', 'Example note 1'),
(null, 'Note 2', 'Example note 2'),
(null, 'Note 3', 'Example note 3');