const { Router } = require('express')
const NoteController = require('../controllers/note.controller')

const controller = new NoteController()

const router = Router()

router.get('/', controller.List)
router.get('/:id', controller.ListOne)
router.post('/', controller.Insert)
router.put('/:id', controller.Update)
router.delete('/:id', controller.Delete)

module.exports = router