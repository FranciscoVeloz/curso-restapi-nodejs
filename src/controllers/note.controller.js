const pool = require('../lib/database')
const Note = require('../models/note.model')

class NoteController {
    async List (req, res) {
        const data = await pool.query('select * from notes')
        res.json(data)
    }

    async ListOne (req, res) {
        const { id } = req.params
        const data = await pool.query('select * from notes where id = ?', [id])
        res.json(data[0])
    }

    async Insert (req, res) {
        const { title, description } = req.body
        const newNote = new Note(title, description)
        await pool.query('insert into notes set ?', [newNote])
        res.json({ status: 'ok' })
    }

    async Update (req, res) {
        const { id } = req.params
        const { title, description } = req.body
        const newNote = new Note(title, description)
        await pool.query('update notes set ? where id = ?', [newNote, id])
        res.json({ status: 'ok' })
    }

    async Delete (req, res) {
        const { id } = req.params
        await pool.query('delete from notes where id = ?', [id])
        res.json({ status: 'ok' })
    }
}

module.exports = NoteController